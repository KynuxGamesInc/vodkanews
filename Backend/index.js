const express = require('express')
const app = express()
var cors = require('cors')
const {Builder, By, Key, until} = require('selenium-webdriver');

app.use(cors());

app.get('/news', async function (req, res) {
    const news_dinges = [];

        const driver_main = await new Builder().forBrowser('chrome').build();
        const driver_detail = await new Builder().forBrowser('chrome').build();
    
        await driver_main.get('https://www.reuters.com/world/europe/');

        const title_elements = await driver_main.findElements(By.xpath(".//a[contains(@class, 'text__text')][contains(@class, 'media-story-card')]"));

        for(var title_element_index in title_elements){
            const title_element = title_elements[title_element_index];
            const title_text_raw = await title_element.getText();

            const title = /^.*/g.exec(title_text_raw)[0];
            
            const detail_path = await title_element.getAttribute("href");

            const detail_url = `${detail_path}`;

            await driver_detail.get(detail_url);

            const content = await driver_detail.findElement(By.xpath(".//div[contains(@class, 'article-body__content')]")).getText();

            const status = "alert";

            // console.log("BLOE", JSON.stringify());

            news_dinges.push({title, content, status});
        }

        // console.log("BLA", x.map(y => y.getText()));
    
        await driver_main.quit();
        // await driver_detail.quit();

  res.send(news_dinges);
})

app.listen(3001)
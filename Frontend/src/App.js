import './App.css';
import axios from 'axios';
import {useEffect, useState} from 'react'; 
import NewsItem from './NewsItem';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import { TableBody } from '@mui/material';


function App() {
  const [news_items, setNewsItems] = useState([]);

  const getNewsItems = async () => {
    const response = await axios.get('http://localhost:3001/news');

    const _news_items = [...news_items];

    for(let _news_item_index in response.data){
      _news_items.push(response.data[_news_item_index]);
    }

    setNewsItems(_news_items);
  }

  useEffect(() => {
    setTimeout(() => {

      getNewsItems();

    }, 0);
  }, []);

  return (
    <Container className="App">
        <Typography variant="h1" component="h2" color={"common.white"}>
          Vodka News
        </Typography>
        
        {news_items.length === 0 && (<Typography variant="span" component="body1" color={"common.white"}>Hacking reuterkes</Typography>)}

        <Container style={{display: "flex", flexDirection: "column"}}>

        {news_items.map((news_item, i) => (<NewsItem key={i} news_item={news_item} i={i}></NewsItem>))}
     
        </Container>
      
    </Container>
  );
}




export default App;


import {useEffect, useState} from 'react'; 
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';


function NewsItem(params) {
    const {news_item, i} = params;

  const [toggle, setToggle] = useState(false);

  const {
    title,
    content,
    status
  } = news_item;

  const do_toggle = () =>{
      setToggle(!toggle);
  }

  return(
    <Container style={{display: "flex", flex: 1, flexDirection: "column", userSelect: "false"}}>
      <Typography variant="h5" component="h3" color={"green"} onClick={do_toggle}>
        {title}
        </Typography>
     <Container style={toggle ? {} : {display: "none"}}>
      <Typography variant="span" component="body1" color={"greenyellow"}>{content} {'\n'}</Typography>
  </Container>
    </Container>
  );
}




export default NewsItem;
